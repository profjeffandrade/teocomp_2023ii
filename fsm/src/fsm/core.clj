(ns fsm.core
  (:gen-class)
  (:require [clojure.set :as set]
            [clojure.pprint :as pp]))


;; Os AFD serão representados por mapas contendo as
;; chaves :states, :alphabet, :transitions, :initial, :accepting. Qualquer valor
;; pode ser usado para identificar os estados e os caracteres do alfabeto, na
;; aula do dia 2023-09-04 eu havia usado strings para identificar os estados,
;; mas achei melhor mudar para keywords para facilitar a escrita.

(def m1 {:states #{:q0 :q1 :q2}
         :alphabet #{0 1}
         :transitions {[:q0 0] :q0,
                      [:q0 1] :q1,
                      [:q1 0] :q2,
                      [:q1 1] :q1,
                      [:q2 0] :q1,
                      [:q2 1] :q1}
         :initial :q0
         :accepting #{:q1}
         })


(defn trex
  "Implementa a *função de transição extendida* para o autômato `m`. Dado um
  autômato `m`, retorna em qual estado o autômato terminará a execução após
  processar a cadeia `w` (uma coloeção, i.e., lista ou array) iniciando no
  estado `qi`."
  [m qi w]
  (cond (empty? w) qi
        :else (let [x (first w)
                    v (rest w)]
                (trex m (get (:transitions m) [qi x]) v))))


;; Abaixo há um outro exemplo de implementação da função de transição extendida,
;; desta vez usando closure e recursão de calda.

(defn make-trex
  "Dado um autômato `m`, retorna a *função de transição extendida* específica
  para este autômato. A função de transição extendida recebe dois arqugmentos,
  `qi` e `w`, e retorna em qual estado o autômato terminará a execução após
  processar a cadeia `w` (uma coloeção, i.e., lista ou array) iniciando no
  estado `qi`."
  [m]
  (fn [qi w]
    (cond (empty? w) qi
          :else (let [x (first w)
                      xs (rest w)
                      tr (:transitions m)
                      qj (tr [qi x])]
                  (recur qj xs)))))


(defn accept?
  "Função que determina se o autômato `m` aceita ou não a cadeia `w`."
  [m w]
  (let [qi (:initial m)
        qj (trex m qi w)
        accepting (:accepting m)]
    (contains? accepting qj)))


(defn make-accept
  "Deado um autômato `m`, retorna a *função de aceitação* específica para este
  autômato. A função de aceitação recebe uma cadeia de caracteres `w` e
  determina se o autômato aceita a cadeia `w` ou não."
  [m]
  (fn [w]
    (let [qi (:initial m)
          trex (make-trex m)
          qj (trex qi w)
          accepting (:accepting m)]
      (contains? accepting qj))))


(def m1-input-examples
  "Exemplo de cadeias de entrada para o autômato `m1` para serem usados o programa
  principal."
  [[1]
   [0]
   [0 0]
   [0 1]
   [0 1 0]
   [1 0 1]
   [0 0 1 1 0 0]])

(defn -main
  [& args]
  (println (format "Autômato m1 => %s\n" (pp/write m1 :stream nil)))
  (println "*** Testes da Função de Transição Extendida ***\n")
  (let [m1trex (make-trex m1)]
    (doseq [w m1-input-examples]
      (doseq [qi [:q0 :q2]]
        (let [qa (trex m1 qi w)
              qb (m1trex qi w)]
          (println (format "Teste :: (trex m1 %s %s) ==> %s" qi w qa))
          (println (format "Teste :: (m1trex %s %s)  ==> %s" qi w qb))))))
  (println "\n*** Testes da Função de Aceitação ***\n")
  (let [m1accept? (make-accept m1)]
    (doseq [w m1-input-examples]
      (let [accept1 (accept? m1 w)
            accept2 (m1accept? w)]
        (println (format "Teste :: (accept? m1 %s) ==> %s" w accept1))
        (println (format "Teste :: (m1accept? %s)  ==> %s" w accept2))))))

